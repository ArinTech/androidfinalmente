package clase.jsonparse;

import java.io.Serializable;



public class CommentBlog implements Serializable{


    //Strings
    private String id_category;
    private String name_category;
    private String description;

    public CommentBlog(String id_category, String name_category, String description) {
        this.name_category = name_category;
        this.id_category = id_category;
        this.description = description;

    }


    //Getters and setters
    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getName_category() {
        return name_category;
    }

    public void setName_category(String name_category) {
        this.name_category = name_category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}