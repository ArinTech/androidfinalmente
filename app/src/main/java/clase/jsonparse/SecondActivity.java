package clase.jsonparse;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

public class SecondActivity extends AppCompatActivity {

    TextView editText_comment,editText_name;
    Button btn_back;
    Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //FindviewByIds
        editText_name = (TextView) findViewById(R.id.editText_name);
        editText_comment = (TextView) findViewById(R.id.editText_comment);
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_save = (Button) findViewById(R.id.btn_save);



        //Save Button
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                new SendDataPOST().execute(editText_name.getText().toString(), editText_comment.getText().toString());

            }
        });

        //Exit Button
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

        //Save new comments, and go back to updated MainActivity class
        public class SendDataPOST extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg) {
                String x = Utils.doPost(arg[0],arg[1]);
                return x;
            }

            @Override
            protected void onPostExecute(String retString) {
                super.onPostExecute(retString);

                try {

                    if (retString.indexOf("OK") > 0 ) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);

                        startActivity(i);
                    }

                }catch (Exception e) {
                    Log.d("comment",e.getMessage());
                }

            }
        }
}
